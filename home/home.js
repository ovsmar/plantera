//#paginated_gallery on recupere l'element avec les bouton pour le deplacement pc
const gallery = document.querySelector("#paginated_gallery");

//.gallery_scroller on recupere l'element avec sa class et cette element est le scroll value X
const gallery_scroller = gallery.querySelector(".gallery_scroller");

//div on recupere toutes tailles des div que le client voit et dans notre element avec le scroll value
const gallery_item_size = gallery_scroller.querySelector("div").clientWidth;

//ici on n'a rajouter les event listener pour que quand on click
gallery.querySelector(".btn.next").addEventListener("click", scrollToNextPage);
gallery.querySelector(".btn.prev").addEventListener("click", scrollToPrevPage);

//cette fonction nous sert a avancer dans le scroller X
function scrollToNextPage() {
  //ici nous prennons la div .gallery_scroller et on ammene la barre de scroll sur le prochain element
  //grace a leur taille nous pouvons savoir ou mettre le scroll pour que les prochain produit s'affiche
  gallery_scroller.scrollBy(gallery_item_size, 0);
}
//cette fonction nous sert a reculer dans le scroller X
function scrollToPrevPage() {
  //ici nous prennons la div .gallery_scroller et on ammene la barre de scroll sur le ancien element
  //grace a leur taille nous pouvons savoir ou mettre le scroll pour que les ancien produit s'affiche
  gallery_scroller.scrollBy(-gallery_item_size, 0);
}

// function updateAlignment(event) {
//   const alignment = event.target.value;
//   for (item of gallery.querySelectorAll(".gallery_scroller > div"))
//     item.style.scrollSnapAlign = alignment;
// }
