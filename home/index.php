<?php   
    //  include "../debug/debug.php";
?>
<head>
    <title>Home</title>
</head>
<?php include "../header/headerAccueil.php"; ?>
<link rel="stylesheet/less" type="text/css" href="../boutique/soldeAnimation.less" />


<h2 class="king">En ce moment</h2>


<?php 
       include "../BDD/data.php";
        $list_pro = getProduit();
        $solde = getProduitAndImageAndSolde()
      ?>


<div id="paginated_gallery" class="gallery margin">

    <div class="gallery_scroller">
    <?php
            foreach($list_pro as $select){
                if($select['stock'] !== 0){
        ?>

        <div class="colored_card">
            <figure class="snip1369 green">
                <?php
            foreach (getimage($select['id']) as $image){
        ?>
                <img class="imgSlider" src="../stock/uploads/<?php echo $image['url']?>">

                <div class="image"><img class="imgSlider" src="../stock/uploads/<?php echo $image['url']?>">

                </div>
                <?php } ?>


                <figcaption>
                    <h3 class="nom"><?php echo $select['nom'];?></h3>

                    <?php if($select['si_solde'] == 1){
                        echo $select['prix'].'$';
                        }else{
                            print_r ('<h6 style="text-decoration-line: line-through;">'.$select['prix'].'$</h6>') & print_r ('<h6 style="color:red">New price! '.$select['prix_solde'].'$</h6>');}?>

                    <!-- <p><?php echo $select['dateproduct'];?></p> -->
                    <p class="descriptionMini"><?php echo $select['descriptionMini'];?></p>
                </figcaption>
                <span class="read-more">Read More <i class="ion-android-arrow-forward"></i></span>
                <a href="../view_product/view_product.php?product=<?php echo $select['id'] ?>"></a>

            </figure>
        </div>
        <?php }} ?>

    </div>
    <span class="btn prev"></span>
    <span class="btn next"></span>
</div>




<div class="ligne_texte">
    <hr class="ligne2">
    <hr class="ligne1">
    <hr class="ligne">
    <hr class="ligne1">
    <hr class="ligne2">
</div>
<div class="text">
    <p class="koho text-grid-1">The standard Lorem Ipsum passage, used since the 1500s
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
        laborum."Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC
        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
        aperiam, eaque ipsa quae ab
    </p>
    <p class="koho text-grid-2">
        dislikes, or avoids pleasure itself, because it is
        pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are
        extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because
        it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great
        pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain
        some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has
        no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"
    </p>
</div>

<h2 class="soldesH1">SOLDES!</h2>

<div id="cardanimation-tdv">
    <div class="card-container">
        <div class="card">
            <div class="card__face card__face--front front1">S</div>
            <div class="card__face card__face--back back1"><span>Jusqu'à</span></div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front2">O</div>
            <div class="card__face card__face--back back2">-</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front3">L</div>
            <div class="card__face card__face--back back3">6</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front4">D</div>
            <div class="card__face card__face--back back4">5</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front5">E</div>
            <div class="card__face card__face--back back5">%</div>
        </div>
        <div class="card">
            <div class="card__face card__face--front front6">S</div>
            <div class="card__face card__face--back back6">!</div>
        </div>
    </div>
</div>


<div id="paginated_gallery" class="gallery margin">

    <div class="gallery_scroller">
        <?php
            foreach($solde as $select){
        ?>

        <div class="colored_card">
            <figure class="snip1369 green">
                <?php
            foreach (getimage($select['id']) as $image){
        ?>
                <img class="imgSlider" src="../stock/uploads/<?php echo $image['url']?>">

                <div class="image"><img class="imgSlider" src="../stock/uploads/<?php echo $image['url']?>">

                </div>
                <?php } ?>


                <figcaption>
                    <h3 class="nom"><?php echo $select['nom'];?></h3>

                    <?php if($select['si_solde'] == 0){
                       print_r ('<h6 style="text-decoration-line: line-through;">'.$select['prix'].'$</h6>') & print_r ('<h6 style="color:red">New price! '.$select['prix_solde'].'$</h6>');}?>



                    <!-- <p><?php echo $select['dateproduct'];?></p> -->
                    <p class="descriptionMini"><?php echo $select['descriptionMini'];?></p>
                </figcaption>
                <span class="read-more">Read More <i class="ion-android-arrow-forward"></i></span>
                <a href="../view_product/view_product.php?product=<?php echo $select['id'] ?>"></a>

            </figure>
        </div>
        <?php } ?>

    </div>
    <span class="btn prev"></span>
    <span class="btn next"></span>
</div>



<div class="banner">
    <div class="banner-me">
        <div class="text-box-wrapper">
            <h4 class="title">Pour la Terre et les Hommes</h4>
            <p>On ne reste pas <b>planté</b> sans rien faire</p>
            <p>pour le développement durable !</p>
            <a class="banner-button" href="../actualites/actualites.php">Suivre notre
                actualité</a>
        </div>
    </div>
</div>





<script src="home.js"></script>
<script src="https://cdn.jsdelivr.net/npm/less@4"></script>
<link rel="stylesheet" href="../gdpr-cookie-consent-popup/cookie/css/style.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="../gdpr-cookie-consent-popup/popupConsent.min.js"></script>
<script>
var cookieConsentOptions = {
    // Cookie usage prevention text
    textPopup: 'We use cookies/targeted advertising to ensure you have the best experience on our site. If you continue to use our site, we will assume that you agree to their use. For more information, please see our <a href="../gdpr-cookie-consent-popup/cookie/PrivacyPolicy.html">privacy policy</a>.',
    // The text of the accept button
    textButtonAccept: 'Accept all',
    // The text of the configure my options button
    textButtonConfigure: 'Configuring choices',
    // The text of the save my options button
    textButtonSave: 'Save choices',
    // The text of the first parameter that the user can define in the "configuration" section.
    authorization: [{
            textAuthorization: 'Allow access to geolocation data',
            nameCookieAuthorization: 'autoriseGeolocation'
        },
        {
            textAuthorization: 'Allow personalised ads and content, ad measurement and audience analysis',
            nameCookieAuthorization: 'targetedAdvertising'
        },
        {
            textAuthorization: 'Storing and/or accessing information on a device',
            nameCookieAuthorization: 'cookieConsent'
        }
    ]
}
popupConsent(cookieConsentOptions);
</script>


<?php include "../footer/footer.php"; ?>



