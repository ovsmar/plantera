<?php 

include '../BDD/data.php';
include '../debug/debug.php';
$error = [];
    if($_POST['title'] !== "" && isset($_POST['title'])){
        $title = $_POST['title'];
    }else{
        array_push($error,'Titre manquant');
    }
    if($_POST['sous_title'] !== "" && isset($_POST['sous_title'])){
        $sous_title = $_POST['sous_title'];
    }else{
        array_push($error,'Sous-Titre manquant');
    }
    if($_POST['card_description'] !== "" && isset($_POST['card_description'])){
        $card_description = $_POST['card_description'];
    }else{
        array_push($error,'Description manquant');
    }
        $mots_cles1 = $_POST['mots_cles1'];
        $mots_cles2 = $_POST['mots_cles2'];
        $mots_cles3 = $_POST['mots_cles3'];
        $paragraph1 = $_POST['paragraph1'];
        $paragraph2 = $_POST['paragraph2'];
        $paragraph3 = $_POST['paragraph3'];

    if($_POST['author'] !== "" && isset($_POST['author'])){
        $author = $_POST['author'];
    }else{
        array_push($error,'Auteur manquant');
    }
    
    if (move_uploaded_file($_FILES['photo']['tmp_name'], "../stock/uploads/".$_FILES['photo']['name'])) {
        print "Image téléchargé avec succès!";
        $photo = $_FILES['photo']['name'];
    } else {
        array_push($error,"Échec du téléchargement de l'image !");
    }
    if(count($error) === 0){
       addActualites($title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author);
       header('location:../admin/espace_admin.php#actualites');
    }else{
        echo '
        <div style="display: flex;align-items: center;height: 50px;justify-content: space-evenly;"><h3 style="color:red;font-size:50px;">/!\</h3><h1>Error : Données Invalides</h1><h3 style="color:red;font-size:50px">/!\</h3></div>
        <div style="display: flex;flex-direction: column;justify-content: flex-end;margin: 50px;width: 90%;border: solid;padding: 10px;">
        ';
        $i = 0;
        foreach($error as $select){
            echo '<strong style="color:red">Erreur n°'.$i.'</strong><p>'.$select.'</p>';
            $i++;
        }
        echo '</div><a href="'.$_SERVER['HTTP_REFERER'].'#ajouter-produit">retour sur le formulaire</a>';
    }
    