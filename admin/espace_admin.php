<?php
session_start();
include '../BDD/data.php';
include '../protected/protectedAdmin.php';
// include "../debug/debug.php";
$compte = $_SESSION['compte'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace Admin</title>
    <link rel="stylesheet" href="espace_admin.css">
    <link rel="stylesheet/less" type="text/css" href="dashboard.less" />
    <link rel="stylesheet" href= "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.css">
</link>
</head>

<body>
    <div class=" admin-panel clearfix">
        <div class="slidebar">
            <div class="logo">
                <a href="../home/index.php"></a>
            </div>
            <ul>
                <li><a href="#dashboard" id="targeted">Dashboard</a></li>
                <li><a href="#ajouter-produit">Ajouter un produit</a></li>
                <!-- <li><a href="#modifier-produit">Modifier un produit</a></li> -->
                <li><a href="#voir-produit">Voir Produit</a></li>
                <li><a href="#voir-commande">Voir commande</a></li>
                <li><a href="#Catégorie">Catégorie</a></li>
                <li><a href="#Messages">Messages</a></li>
                <li><a href="#actualites">Ajouter des Actualites</a></li>
                <li><a href="#voir-actualites">voir les Actualites</a></li>
                <!-- <li><a href="#modifier-actualites">Modifier les Actualites</a></li> -->
                <li><a href="../sessiondelete/sessiondelete.php">Deconnexion</a></li>
            </ul>
        </div>

        <div class="main">
            <div class="mainContent clearfix">
                <div id="dashboard">
                    <h2 class="header"><span class="icon"></span>Dashboard</h2>
                    <div class="welcome-card">
                        <?php foreach (selectAdminNom($compte['pseudo']) as $select) { ?>
                        <h1>Bienvenue <strong><?php echo $select['pseudo']; ?></strong></h1>
                        <?php } ?>
                        <p>
                            ✌️Hey, je vous souhaite la bienvenue sur votre espace admin.
                        </p>
                    </div>

                    <div class="container">
                <?php foreach(TotalProduits() as $produits){ ?>

  <div class="panel post">
    <a href="#voir-produit"><span><?php echo $produits['total']; ?></span>Total produits</a>
  </div>
  <?php } ?>
  <?php foreach(TotalClients() as $clients){ ?>
  <div class="panel comment">
    <a href="#"><span><?php echo $clients['total']; ?></span>Total client</a>
    <?php } ?>
  </div>
  <?php foreach(TotalCommandes() as $commandes){ ?>
  <div class="panel page">
    <a href="#voir-commande"><span><?php echo $commandes['total']; ?></span>Total commandes</a>
    <?php } ?>
  </div>
  <?php foreach(TotalPosts() as $posts){ ?>
  <div class="panel user">
    <a href="#voir-actualites"><span><?php echo $posts['total']; ?></span>Total posts</a>
    <?php } ?>
  </div>
  <?php foreach(TotalCategorie() as $categories){ ?>
  <div class="panel category">
    <a href="#Catégorie"><span><?php echo $categories['total']; ?></span>Total categories</a>
    <?php } ?>
  </div>
  <?php foreach(TotalMessages() as $messages){ ?>
  <div class="panel message">
    <a href="#Messages"><span><?php echo $messages['total']; ?></span>Total messages</a>
    <?php } ?>
  </div>
  
  

</div>

                </div>
                <div id="ajouter-produit">
                    <h2 class="header">Ajouter produit</h2>
                    <form class="form-size" action="../controller/insert.php" method="post"
                        enctype="multipart/form-data">
                        <div class="div-input-product">
                            <label for="nom" class="label-product"></label>
                            <input type="text" placeholder="Nom du produit" name="nom" class="input-product">
                        </div>
                        <div id="div1" class="grid">
                            <div class="plus">
                             <button type="button" onclick="ajout('div1')" class="button-plus">+</button>
                            </div>
                        </div>
                        <button type="button" onclick="supp('div1')">tout supprimer</button>
                        <input id='nbrImg' name="nbrImg" type="hidden">
                        <div class="div-input-product">
                            <label for="id" class="label-product">Catégorie :  </label>
                            <select type="text" name="id">
                                <option value="">--Please choose an option--</option>
                                <?php foreach(selectCategorie() as $select){?>
                                <option value="<?php echo $select['id']?>"><?php echo $select['nom']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="div-textarea-product">

                            <label for="description" class="label-product"></label>
                            <textarea name="description" placeholder="Description..." id="mytextarea" cols="80"
                                rows="20" class="textarea-product"></textarea>
                        </div>

                        <div class="div-textarea-product">
                            <label for="Minidescription" class="label-product"></label>
                            <textarea name="Minidescription" cols="80" rows="5"
                                placeholder="Mini Description - 125 caractères maximum!"
                                class="textarea-product"></textarea>
                        </div>

                        <div class="div-input-product">
                            <label for="temperature" class="label-product"></label>
                            <input type="text" placeholder="Temperature" name="temperature" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="humidite" class="label-product"></label>
                            <input type="text" placeholder="Humidite" name="humidite" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="temps" class="label-product"></label>
                            <input type="text" placeholder="Temps" name="temps" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="stock" class="label-product"></label>
                            <input type="number" placeholder="Stock" name="stock" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="prix" class="label-product"></label>
                            <input type="number" placeholder="Prix" step="any" name="prix" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                           
                            <label for="pet-select">Solde:</label>
                            <select name="si_solde" id="disp-select">
                                <option value="1" boolval(1)>NO</option>
                                <option value="0" boolval(0)>YES</option>
                            </select>

                        </div>

                        <div class="div-input-product">
                            <label for="Prixsolde" class="label-product"></label>
                            <input type="number" placeholder="Solde" step="any" name="Prixsolde" id=""
                                class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="livraison" class="label-product">Livraison:</label>
                            <input style="width: 20px;" type="checkbox" name="livraison" id="checkbox-round"
                                class="input-product">
                        </div>

                        <input type="submit" class="btn" value="Ajouter">
                    </form>


                </div>
                <div id="modifier-produit">
                    <h2 class="header">Modifier un produit</h2>
                    <?php
                    $id = $_POST['modif'];
                    if (isset($_POST['modif'])) {
                        $functionGetProduit = getProduitById($id);
                    } else {
                        print "<p class='error'>Veuillez selectionner un produit dans la categorie 'voir produit' et cliquer sur le bouton 'Modifier'</p>";
                    }
                    foreach (getProduitById($id) as $select) {
                    ?>
                    <form class="" action="../controller/update.php" method="post" enctype="multipart/form-data">
                        <p>nom :</p>
                        <input type="text" id="nom" name="nom" class="input-product"
                            value="<?php echo $select['nom']; ?>">

                        <p>img :</p>
                        <div id="div2" class="grid">
                            <div class="plus">
                             <button type="button" onclick="ajout('div2','nbrImg2')" class="button-plus">+</button>
                            </div>
                             <?php for($i = 0;$i<count(getimage($select['id']));$i++){
                                 echo '
                                 <div class="product-pic" name="div">
                                 <label class="-label" name="label" for="file'.$i.'">
                                     <span class="glyphicon glyphicon-camera"></span>
                                     <span>Change Image</span>
                                 </label>
                                 <input name="file'.$i.'" id="file'.$i.'" class="input-file" type="file" onchange="previewPicture(this,'.$i.')">
                                 <input name="file'.$i.'" type="hidden" value="'.getimage($select['id'])[$i]['url'].'">
                                    <img name="img'.$i.'" class="rendu-image-product" for="file" src="../stock/uploads/'.getimage($select['id'])[$i]['url'].'">
                                </div>';
                             }?>
                            
                        </div>
                        <button type="button" onclick="supp('div2')">tout supprimer</button>
                        <input id='nbrImg2' name="nbrImg" type="hidden" value="<?= $i ?>">

                        <div class="div-input-product">
                            <label for="id" class="label-product">Catégorie :  </label>
                            <select type="text" name="id-cat" class ="input-product margin-top">
                                <option value="<?php echo catId($select['id'])[0]['id']?>" style="background:lightgreen">= <?php echo catId($select['id'])[0]['nom']?> </option>
                                <?php for($i = 0;$i<count(selectCategorie());$i++){?>
                                <option value="<?php echo selectCategorie()[$i]['id'] ?>"><?php echo selectCategorie()[$i]['nom']?></option>
                                <?php }?>
                            </select>
                        </div>

                        <p>description :</p>
                        <textarea name="description" id="mytextarea" id="description" cols="80" rows="20"
                            class="textarea-product" value=""><?php echo $select['description']; ?></textarea>

                        <p>Minidescription :</p>
                        <textarea name="Minidescription" id="Minidescription" cols="80" rows="10"
                            class="textarea-product" value=""><?php echo $select['descriptionMini']; ?></textarea>

                        <p>temperature :</p>
                        <input type="text" name="temperature" id="temperature" class="input-product"
                            value="<?php echo $select['temperature']; ?>">

                        <p>humidite :</p>
                        <input type="text" name="humidite" id="humidite" class="input-product"
                            value="<?php echo $select['humidite']; ?>">

                        <p>temps :</p>
                        <input type="text" name="temps" id="temps" class="input-product"
                            value="<?php echo $select['temps']; ?>">

                        <p>stock :</p>
                        <input type="number" name="stock" id="stock" class="input-product"
                            value="<?php echo $select['stock']; ?>">

                        <p>prix :</p>
                        <input type="number" name="prix" id="prix" class="input-product"
                            value="<?php echo $select['prix']; ?>">


                        <p>solde :</p>
                        <select name="si_solde" id="disp-select">
                            <option value="1" boolval(1)>NO</option>
                            <option value="0" boolval(0)>Yes</option>
                        </select>


                        <p>Prixsolde :</p>
                        <input type="number" step="any" name="Prixsolde" id="Prixsolde" class="input-product"
                            value="<?php echo $select['prix_solde']; ?>">

                        <p>livraison :</p>
                        <input type="checkbox" name="livraison" id="livraison" class="input-product"
                            value="<?php echo $select['si_livraison']; ?>">

                        <button class="btn" type="submit" name="id" value="<?php echo $select['id']; ?>">Modifier
                            Produit</button>
                    </form>
                    <?php } ?>
                </div>
                <div id="voir-produit">
                    <h2 class="header">Voir Produit</h2>
                    <div class="containerTable">
                        <div class="form-control">
                            <label for="search"><i class="icon-search"></i></label>
                            <input class="table-filter" type="search" data-table="advanced-web-table"
                                placeholder="Search...">
                            <button onclick="exportData()">
                                <i class="fas fa-download"></i>
                                Download
                            </button>
                        </div>
                        <!--  Table  -->
                        <div class="table-responsive">
                            <table id="ordering-table" class="advanced-web-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>img</th>
                                        <th>nom</th>
                                        <th>description</th>
                                        <th>descriptionMini</th>
                                        <th>temperature</th>
                                        <th>humidite</th>
                                        <th>temps</th>
                                        <th>stock</th>
                                        <th>prix</th>
                                        <th>si_solde</th>
                                        <th>prix_solde</th>
                                        <th>si_livraison</th>
                                        <th>date</th>
                                        <th>modif</th>
                                        <th>sup</th>
                                    </tr>
                                </thead>
                                <?php

                                $list_pro = getProduit();
                                
                                ?>
                                <tbody>
                                    <tr>
                                        <?php
                                        foreach ($list_pro as $select) {
                                            
                                        ?>
                                        <td><?php echo $select['id']; ?></td>
                                        <td><img class="modif-img" src="../stock/uploads/<?php echo getimage($select['id'])[0]['url'] ?>" />
                                        </td>
                                        <td><?php echo $select['nom']; ?></td>
                                        <td style="min-width: 400px;"><?php echo $select['description']; ?></td>
                                        <td><?php echo $select['descriptionMini']; ?></td>
                                        <td><?php echo $select['temperature']; ?></td>
                                        <td><?php echo $select['humidite']; ?></td>
                                        <td><?php echo $select['temps']; ?></td>
                                        <td><?php echo $select['stock']; ?></td>
                                        <td><?php echo $select['prix']; ?></td>
                                        <td><?php echo $select['si_solde']; ?></td>
                                        <td><?php echo $select['prix_solde']; ?></td>
                                        <td><?php echo $select['si_livraison']; ?></td>
                                        <td><?php echo $select['dateproduct']; ?></td>
                                        <td>
                                            <form action="../admin/espace_admin.php#modifier-produit" method="post">
                                                <button type="submit" name="modif"
                                                    value="<?php echo $select['id'] ?>"><i
                                                        class="fas fa-pen"></i></button></button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="../controller/delete.php" method="post">

                                                <button type="submit" name="supp" value="<?php echo $select['id'] ?>"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        </td>



                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="voir-commande">
                    <h2 class="header">Voir commande</h2>
                    <?php 
               
                    foreach(selectAllCommande() as $select){
                        // var_dump($select);
                        echo '<br></br>';
                        
                        ?>
                        
                    <div class="commande">
                        <label for="tableaux-commande-client"><b style="color:gray">Info Client</b></label>
                        <table id="tableaux-commande-client">
                            <thead>
                                <tr>
                                    <th>Id transaction</th>
                                    <th>Numéro de commande</th>
                                    <th>Nom</th>
                                    <th>Email</th>
                                    <th>Numéro de telephone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b><?php echo $select['id_transaction']?></b></td>
                                    <td><b><?php echo $select['order_number']?></b></td>
                                    <td><b><?php echo $select['cust_name']?></b></td>
                                    <td><b><?php echo $select['cust_email']?></b></td>
                                    <td><b><?php echo $select['customerPhone']?></b></td>
                                </tr>
                            </tbody>
                        </table>
                        <label for="tableaux-commande-client"><b style="color:gray">Produit commander</b></label>
                        <table id="tableaux-commande-client">
                            <thead>
                                <tr>
                                    <th>Id du produit</th>
                                    <th>Nom du produit</th>
                                    <th>Prix du produit</th>
                                    <th>Quantity du produit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><b><?php echo $select['id_produit']?></b></td>
                                    <td><b><?php echo $select['nom']?></b></td>
                                    <td><b><?php echo $select['item_price']?></b></td>
                                    <td><b><?php echo $select['quantite']?></b></td>
                                </tr>
                            </tbody>
                        </table>
                        <p>Options d'expédition : <b><?php echo $select['mode_livraison']?></b></p>
                        <?php 
                        if($select['mode_livraison'] === 'livraison'){?>
                            <p>Adresse d'expédition : <b><?php echo $select['customerAddress'].', '.$select['customerCity'].' '.$select['customerZipcode'].'. '.$select['customerCountry']?></b></p>
                        <?php } ?>
                         
                        <p>Date prise de la commande : <b><?php echo $select['created']?></b></p>
                        <p>Total : <b><?php echo number_format($select['paid_amount'])?>$</b></p>

                            
                        <div class=" payment_status_flex">
                            <p>
                                Paiement Accepter :<?php if($select['payment_status'] === "succeeded"){ echo '<div class="payment_status_green"></div>';}else{echo '<div class="payment_status_red"></div>';} ?>
                            </p>
                        </div>
                    </div>


                    <?php } ?>

                </div>
                <div id="Catégorie">
                    <h2 class="header">Catégorie</h2>
                    <div class="ajouter-categorie">
                        <p><strong>Ajouter une catégorie</strong></p>
                        <form action="../controller/Catégorie/create.php" method="post" style="display: flex;flex-direction: column;">
                            <label>Nom de la catégorie : </label>
                            <input type="text" name="nom" class="input-product margin-top">
                            <label>Description de la catégorie (optionel): </label>
                            <input type="text" name="description" class="input-product margin-top">
                            <input type="submit" value="Créer" class="submit">
                        </form>
                    </div>
                    <div class="update-categorie">
                        <p><strong>Mettre a jour une catégorie</strong></p>
                        <form action="../controller/Catégorie/update.php" method="post" style="display: flex;flex-direction: column;">
                            <select type="text" name="id" id="select" class="input-product margin-top">
                                <option value="">--Please choose an option--</option>
                                <?php foreach(selectCategorie() as $select){?>
                                <option value="<?php echo $select['id']?>"><?php echo $select['nom']?></option>
                                <?php }?>
                            </select>
                            <div id="test">
                                <?php
                                $i=0;foreach(selectCategorie() as $select){?>
                                    <input type="hidden" value="<?php echo $select['nom']?>" name="select-nom<?php echo $select['id']?>" id="<?php echo $i;$i++ ?>" class="input-product margin-top">
                                    <input type="hidden" value="<?php echo $select['description']?>" name="select-desc<?php echo $select['id']?>" id="<?php echo $i;$i++ ?>" class="input-product margin-top">
                                <?php }?>
                                <input type="hidden" value="<?php echo $i - 1?>" id="tour">
                                <input type="submit" value="modifier" class="submit">
                            </div>
                        </form>
                        <script>
                            document.getElementById('select').addEventListener('change', function handleChange(event) {
                                for(let i = 0;i <= document.getElementById('tour').value;i++){
                                    document.getElementById(i).type = 'hidden'
                                }
                                document.getElementsByName("select-nom"+event.target.value)[0].type = "text"
                                document.getElementsByName("select-desc"+event.target.value)[0].type = "text"
                            });
                        </script>
                    </div>
                    <div class="delete-categorie">
                        <p><strong>Supprimer une categorie</strong></p>
                        <form action="../controller/Catégorie/delete.php" method="post" style="display: flex;flex-direction: column;">
                            <select type="text" name="id" id="select" class="input-product margin-top">
                                <option value="">--Please choose an option--</option>
                                <?php foreach(selectCategorie() as $select){?>
                                <option value="<?php echo $select['id']?>"><?php echo $select['nom']?></option>
                                <?php }?>
                            </select>
                            <input type="submit" value="Supprimer" class="submit">
                        </form>
                    </div>
                </div>

                
                <div id="Messages">
                    <h2 class="header">Messages</h2>
                    <?php

                    $list_mail = getAllMail();

                    ?>


                    <table class="GeneratedTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nom</th>
                                <th>email</th>
                                <th>comment</th>
                                <th>Date</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($list_mail as $f) {
                            ?>
                            <tr>
                                <td><?php echo $f["id"] ?></td>
                                <td><?php echo $f["nom"] ?></td>
                                <td><a href="mailto: abc@example.com"><?php echo $f["email"] ?></a></td>
                                <td><?php echo $f["comment"] ?></td>
                                <td><?php echo $f["datemail"]  ?></td>
                                <td>
                                    <div class="delt">
                                        <form class="test" action="../controller/deleteMail.php" method="post">

                                            <button type="submit" name="supp" value="<?php echo $f['id'] ?>"><i
                                                    class="fas fa-trash"></i></button>
                                        </form>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>


                <div id="actualites">
                    <h2 class="header">Actualites</h2>
                    <form class="form-size" action="../controller/insertActualites.php" method="post"
                        enctype="multipart/form-data">

                        <div class="div-input-product">
                            <label for="photo" class="label-product">photo:</label>
                            <input class="fichier" name="photo" type="file" accept="image/*">
                        </div>

                        <div class="div-input-product">
                            <label for="title" class="label-product"></label>
                            <input type="text" placeholder="Title" name="title" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="sous_title" class="label-product"></label>
                            <input type="text" placeholder="Sous title" name="sous_title" class="input-product">
                        </div>
                        <div class="div-textarea-product">
                            <label for="card_description" class="label-product"></label>
                            <textarea name="card_description" placeholder="Card description" cols="80" rows="5"
                                class="textarea-product"></textarea>
                        </div>
        
                        <div class="div-input-product">
                            <label for="mots_cles1" class="label-product"></label>
                            <input type="text" placeholder="mots cles1" name="mots cles1" class="input-product">
                        </div>
                        <div class="div-input-product">
                            <label for="mots_cles2" class="label-product"></label>
                            <input type="text" placeholder="mots cles2" name="mots_cles2" class="input-product">
                        </div>
                        <div class="div-input-product">
                            <label for="mots_cles3" class="label-product"></label>
                            <input type="text" placeholder="mots cles3" name="mots_cles3" class="input-product">
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph1" class="label-product"></label>
                            <textarea name="paragraph1" placeholder="paragraph1" id="mytextarea" cols="80" rows="20"
                                class="textarea-product"></textarea>
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph2" class="label-product"></label>
                            <textarea name="paragraph2" placeholder="paragraph2" id="mytextarea" cols="80" rows="20"
                                class="textarea-product"></textarea>
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph3" class="label-product"></label>
                            <textarea name="paragraph3" placeholder="paragraph3" id="mytextarea" cols="80" rows="20"
                                class="textarea-product"></textarea>
                        </div>

                        <div class="div-input-product">
                            <label for="author" class="label-product"></label>
                            <input type="text" placeholder="Author" name="author" class="input-product">
                        </div>

                        <input class="btn" type="submit" value="Ajouter">
                    </form>
                </div>


                <div id="voir-actualites">
                    <h2 class="header">Voir-actualites</h2>

                    <div class="containerTable">
                        <div class="form-control">
                            <label for="search"><i class="icon-search"></i></label>
                            <input class="table-filter" type="search" data-table="advanced-web-table"
                                placeholder="Search...">
                        </div>
                        <!--  Table  -->
                        <div class="table-responsive">
                            <table id="ordering-table" class="advanced-web-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>photo</th>
                                        <th>title</th>
                                        <th>sous_title</th>
                                        <th>card_description</th>
                                        <th>mots_cles1</th>
                                        <th>mots_cles2</th>
                                        <th>mots_cles3</th>
                                        <th>paragraph1</th>
                                        <th>paragraph2</th>
                                        <th>paragraph3</th>
                                        <th>author</th>
                                        <th>Date</th>
                                        <th>modif</th>
                                        <th>sup</th>
                                    </tr>
                                </thead>
                                <?php

                                $list_act = getActualites();

                                ?>
                                <tbody>
                                    <tr>
                                        <?php
                                        foreach ($list_act as $select) {
                                        ?>
                                        <td><?php echo $select['id']; ?></td>
                                        <td><img class="modif-img"
                                                src="../stock/uploads/<?php echo $select['photo'] ?>" />
                                        </td>
                                        <td><?php echo $select['title']; ?></td>
                                        <td><?php echo $select['sous_title']; ?></td>
                                        <td style="min-width: 400px;"><?php echo $select['card_description']; ?>
                                        </td>
                                        <td><?php echo $select['mots_cles1']; ?></td>
                                        <td><?php echo $select['mots_cles2']; ?></td>
                                        <td><?php echo $select['mots_cles3']; ?></td>
                                        <td style="min-width: 400px;"><?php echo $select['paragraph1']; ?></td>
                                        <td style="min-width: 400px;"><?php echo $select['paragraph2']; ?></td>
                                        <td style="min-width: 400px;"><?php echo $select['paragraph3']; ?></td>
                                        <td><?php echo $select['author']; ?></td>
                                        <td><?php echo $select['Ladate']; ?></td>
                                        <td>
                                            <form action="../admin/espace_admin.php#modifier-actualites" method="post">
                                                <button type="submit" name="modif"
                                                    value="<?php echo $select['id'] ?>"><i
                                                        class="fas fa-pen"></i></button></button>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="../controller/deleteActualites.php" method="post">

                                                <button type="submit" name="supp" value="<?php echo $select['id'] ?>"><i
                                                        class="fas fa-trash"></i></button>
                                            </form>
                                        </td>



                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div id="modifier-actualites">
                    <h2 class="header">Modifier actualites</h2>
                    <?php
                    $id = $_POST['modif'];
                    if (isset($_POST['modif'])) {
                        $functionGetProduit = getctualitesById($id);
                    } else {
                        print "<p class='error'>Veuillez selectionner un element dans la categorie 'voir Actualites' et cliquer sur le bouton 'Modifier'</p>";
                    }
                    foreach ($functionGetProduit as $select) {
                    ?>
                    <form class="" action="../controller/updateActualites.php" method="post"
                        enctype="multipart/form-data">

                        <p>photo :</p>
                        <input type="file" id="photo" name="photo" type="file" accept="image/*" class="input-carousel">

                        <input type="hidden" name="photo" value="<?php echo $select['photo']; ?>">
                        
                        <p>title :</p>
                        <input type="text" id="title" name="title" class="input-product"
                            value="<?php echo $select['title']; ?>">

                        <p>sous_title :</p>
                        <input type="text" name="sous_title" id="sous_title" class="input-product"
                            value="<?php echo $select['sous_title']; ?>">

                        <p>card_description :</p>
                        <textarea name="card_description" id="mytextarea" id="card_description" cols="80" rows="20"
                            class="textarea-product" value=""><?php echo $select['card_description']; ?></textarea>


                        <p>mots_cles1:</p>
                        <input type="text" name="mots_cles1" id="mots_cles1" class="input-product"
                            value="<?php echo $select['mots_cles1']; ?>">

                        <p>mots_cles2:</p>
                        <input type="text" name="mots_cles2" id="mots_cles2" class="input-product"
                            value="<?php echo $select['mots_cles2']; ?>">

                        <p>mots_cles3:</p>
                        <input type="text" name="mots_cles3" id="mots_cles3" class="input-product"
                            value="<?php echo $select['mots_cles3']; ?>">


                        <p>paragraph1 :</p>
                        <textarea name="paragraph1" id="mytextarea" id="paragraph1" cols="80" rows="10"
                            class="textarea-product" value=""><?php echo $select['paragraph1']; ?></textarea>

                        <p>paragraph2 :</p>
                        <textarea name="paragraph2" id="mytextarea" id="paragraph2" cols="80" rows="10"
                            class="textarea-product" value=""><?php echo $select['paragraph2']; ?></textarea>

                        <p>paragraph3 :</p>
                        <textarea name="paragraph3" id="mytextarea" id="paragraph3" cols="80" rows="10"
                            class="textarea-product" value=""><?php echo $select['paragraph3']; ?></textarea>

                        <p>author :</p>
                        <input type="text" name="author" id="author" class="input-product"
                            value="<?php echo $select['author']; ?>">



                        <button class="btn" type="submit" name="id" value="<?php echo $select['id']; ?>">Modifier
                            actualite</button>
                    </form>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/less@4"></script>
    <script src="script.js"></script>
    <script src="filter.js"></script>
    <script src="exportData.js"></script>
    <script src="img.js"></script>
    <script src="https://cdn.tiny.cloud/1/5dq712yq5b8l1tr5xrw3lsrhoqrjnegljkpjayhxd17fclk2/tinymce/6/tinymce.min.js"
        referrerpolicy="origin"></script>>

    <script>
    tinymce.init({
        selector: '#mytextarea',
        plugins: [
            'autolink',
             
            'lists', 'link', 'image', 'charmap', 'preview', 'anchor',
            'searchreplace', 'visualblocks',
             'fullscreen',  'insertdatetime', 'media',
            'table', 'help', 'wordcount'
        ],
        toolbar: 'undo redo | formatpainter casechange blocks | bold italic backcolor | ' +
            'alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist checklist outdent indent | removeformat | a11ycheck code table help'
    });
    </script>



    <script>
    </script>
</body>

</html>"