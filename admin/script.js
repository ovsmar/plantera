function ajout(id) {
  if (document.getElementsByName("div").length <= 7) {
    document.getElementById(id).innerHTML += `
        
        <div class="product-pic" name="div">
        <input onclick="supp('${id}')" value="-">
        <label class="-label" name="label" for="file${
          document.getElementsByName("div").length
        }">
            <span class="glyphicon glyphicon-camera"></span>
            <span>Change Image</span>
        </label>
        
        <input name="file${document.getElementsByName("div").length}" id="file${
      document.getElementsByName("div").length
    }" class="input-file" type="file" onchange="previewPicture(this,${
      document.getElementsByName("div").length
    })">

            <img name="img${
              document.getElementsByName("div").length
            }" class="rendu-image-product" for="file">
        
    </div>
        `;
    console.log(document.getElementsByName("div").length);
    document.getElementById("nbrImg").value =
      document.getElementsByName("div").length - 1;
    document.getElementById("nbrImg2").value =
      parseInt(document.getElementById("nbrImg2").value) + 1;
  } else {
    alert("vous ne pouvez pas ajouter plus d'image");
  }
}
function supp(id) {
  document.getElementById(id).innerHTML = `
  <div class="plus">
            <button type="button" onclick="ajout('${id}')" class="button-plus">+</button>
    </div>
  `;
  document.getElementById("nbrImg2").value = 0;
}

// La fonction previewPicture
var previewPicture = function (e, place) {
  // e.files contient un objet FileList
  const [picture] = e.files;
  // "picture" est un objet File
  if (picture) {
    // L'objet FileReader
    var reader = new FileReader();

    // L'événement déclenché lorsque la lecture est complète
    reader.onload = function (e) {
      // On change l'URL de l'image (base64)
      let image = document.getElementsByName("img" + place)[0];
      image.style.width = "100%";
      image.style.height = "100%";
      image.src = e.target.result;
    };

    // On lit le fichier "picture" uploadé
    reader.readAsDataURL(picture);
  }
};

const targetDiv = document.getElementById("span_actu");
const btn = document.getElementById("toggle");
btn.onclick = function () {
  if (targetDiv.style.display !== "none") {
    targetDiv.style.display = "none";
  } else {
    targetDiv.style.display = "block";
  }
};
