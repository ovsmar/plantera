function change() {
  let i = 0;
  //cette variable va nous servir a faire le total du prix de tout les produit
  let total = 0;
  //#recap est la div du recapitulatif de la commande et on la met vide
  document.getElementById("recap").innerHTML = "";
  //product-title sert a recupérer les nom des produit et on fait un foreach sur les nodeElement
  document.getElementsByName("product-title").forEach(
    //#recap est la div du recapitulatif de la commande et on marque le nom du produit x la quantité
    (element) => (
      (document.getElementById("recap").innerHTML += `
    <div class="flex-row" id="">
    <p>${element.textContent} x <b>${
        document.getElementsByName("quantity" + i)[0].value
      }</b></p></div>
    `),
      //total va stocker le prix * la quantité de produit
      (total += Number(
        document.getElementsByName("prix")[i].textContent *
          document.getElementsByName("quantity" + i)[0].value
      )),
      i++
    )
  );
  //ici on re-affiche le total
  document.getElementById("cart-subtotal").innerHTML = `${total.toFixed(2)}`;
}
//et nous lancons la fonction directement pour avoir notre resultat quand on arrive sur la page
change();
