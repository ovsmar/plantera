<?php 
include '../BDD/data.php';
include '../debug/debug.php';
require __DIR__.'/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

$html = '
<style>

body{
    display:flex;
    flex-direction:columns;
    
img{
    width:auto;
    height:100px;
}
.header{
    width:auto;
    height:auto;
    display:flex;
    flex-direction: row;
    margin:0;
}
table
{
    width:  80%;
    border:none;
    border-collapse: collapse;
    margin: auto;
}
th
{
    text-align: center;
    border: solid 1px #eee;
    background: #f8f8f8;
}
td
{
    text-align: center;
    border: solid 1px #eee;
}
</style>
<div class="header">
<img src="../stock/ress/Logo_Plantera.png">
<h1>Factures du '.date('Y-m-d h:i:s', strtotime($_POST['created'])).'</h1>
</div>
<p>Prénom : '.htmlspecialchars(selectClientCommande($_POST['id_utilisateur'])[0]['cust_name']).'</p>
<p>Mail : '.htmlspecialchars(selectClientCommande($_POST['id_utilisateur'])[0]['cust_email']).'</p>
<p>Numéro de téléphone : '.htmlspecialchars(selectClientCommande($_POST['id_utilisateur'])[0]['customerPhone']).'</p>
<p><strong>Total :'.htmlspecialchars(selectClientCommande($_POST['id_utilisateur'])[0]['customerPhone']).'</strong></p>
<h2>Votre commande</h2>
<table>
<col style="width: 33%">
    <col style="width: 33%">
    <col style="width: 33%">
<tr>
<th>Numéro de commande</th>
<th>Montant Payer (hors tva)</th>
<th>Date</th>

</tr>

';
foreach(selectClientCommande($_POST['id_utilisateur']) as $select){
        $html .= '<tr><td>'.htmlspecialchars($select['order_number']).'</td>
        <td>'.htmlspecialchars($select['paid_amount']).'$</td>
        <td>'.htmlspecialchars($select['created']).'</td></tr>';
}

$html .= '

</table>
<h3>Details de la commande</h3>
<table>
<col style="width: 33%">
    <col style="width: 33%">
    <col style="width: 33%">
<tr>
<th>Numéro de commande</th>
<th>Prix du produit</th>
<th>Nom du produit</th>
<th>Options d"expédition</th>	
</tr>
';
foreach(selectClientCommande($_POST['id_utilisateur']) as $select){
        $html .= '<tr><td>'.htmlspecialchars($select['order_number']).'</td>
        <td>'.getProduitById($select['id_produit'])[0]['prix'].'$</td>
        <td>'.htmlspecialchars($select['item_name']).'</td>
        <td>'.htmlspecialchars($select['mode_livraison'] ).'</td></tr>';
}
$html .= '

</table>
<h3>Details des produits</h3>
<table>
<col style="width: 33%height:5%">
    <col style="width: 25%;height:5%">
    <col style="width: 25%height:5%">
    <col style="width: 5%height:5%">
    <col style="width: 25%height:5%">
<tr>
<th>Image du produit</th>
<th>Nom de produit</th>
<th>Prix du produit</th>
<th>Quantité du produit</th>

</tr>
';
foreach(selectClientCommande($_POST['id_utilisateur']) as $select){
        $html .= '<tr><td><img src="../stock/uploads/'.getImage($select['id_produit'])[0]['url'].'" style="width:50px"></td>
        <td>'.getProduitById($select['id_produit'])[0]['nom'].'</td>
        <td>'.getProduitById($select['id_produit'])[0]['prix'].'$</td>
        <td>'.$select['quantite'].'</td></tr>';
        $total += getProduitById($select['id_produit'])[0]['prix'];
}
$html .= '
</table>
<h2><strong>Total :'.htmlspecialchars($select['paid_amount']).'$</strong></h2>
';
$l = 'livraison';
$html2pdf = new Html2Pdf();
$html2pdf->writeHTML($html);
ob_end_clean();
$html2pdf->output();


?>