<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet/less" type="text/css" href="error.less" />
    <title>Document</title>
</head>
<body>
<div class="noise"></div>
<div class="overlay"></div>
<div class="terminal">
  <h1>Error <span class="errorcode"></span></h1>
  <p class="output">Vous n'avez pas la permission d'accéder à cette page .</p>
  <p class="output">Veuillez essayer de <a href="../sign-up-login/Sing-up-login.php">connecter</a> ou <a href="../home/index.php">retourez à la page d'accueil</a>.</p>
</div>  
<script src="https://cdn.jsdelivr.net/npm/less@4"></script>
</body>
</html>