document
  .getElementById("cat")
  .addEventListener("change", function handleChange(event) {
    let cat = document.getElementsByName("cat");
    const solde = document.getElementById("solde");
    const animation = document.getElementById("cardanimation-tdv");
    const list = document.getElementById("list");
    let i = 0;
    if (event.target.value === "0") {
      document.getElementById("aucun").innerHTML = "sélectionner par catégorie";
      document.getElementById("list").style.display = "grid";
      document.getElementById("cardanimation-tdv").style.display = "grid";
      document.getElementById("solde").style.display = "grid";
      for (
        let i = 0;
        i <= document.getElementById("list").children.length;
        i++
      ) {
        document.getElementById(i).style.display = "grid";
      }
    } else if (event.target.value === "4") {
      document.getElementById("list").style.display = "none";
      document.getElementById("cardanimation-tdv").style.display = "grid";
      document.getElementById("solde").style.display = "grid";
    } else {
      solde.style.display = "none";
      list.style.display = "grid";
      animation.style.display = "none";

      cat.forEach(function (e) {
        if (e.value !== event.target.value) {
          document.getElementById(i).style.display = "none";
        } else {
          document.getElementById(i).style.display = "grid";
        }
        i++;
      });
      document.getElementById("aucun").innerHTML = "aucune catégorie";
    }
  });
