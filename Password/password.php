<?php 
session_start();
// var_dump($_SESSION[$_GET['ref']]);
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Reset Password</title>
</head>

<body>
    <!-- <div class="div-form-reset" id="background_div">
        <form method='post' action='../controller/reset-password.php' class='form-reset'>
            <label>Nouveaux mot de passe :</label>
            <input type='text' name='new-password' class="input-reset">
            <input type='submit'>
        </form>
    </div> -->



<?php 
if($_GET['ref']){
?> 
    <form method='post' action='../controller/reset-password.php' style="max-width:500px;margin:auto">
      
      <img class="planteraImg" src="../stock/ress/Logo_Plantera.png">
            <h2><span class="fa-passwd-reset fa-stack"><i class="fa fa-undo fa-stack-2x"></i><i class="fa fa-lock fa-stack-1x"></i></span>Réinitialisez votre mot de passe<span class="fa-passwd-reset fa-stack"><i class="fa fa-undo fa-stack-2x"></i><i class="fa fa-lock fa-stack-1x"></i></span></h2>

        <div class="input-container"><i class="fa fa-key icon"></i>
            <input class="input-field" id="password-1" type="password" placeholder="Tapez votre nouveau mot de passe" name="password" oninput='validate();'>
        </div>
       
        <span id="pwd-length-1"></span>
    
        <div class="input-container"><i class="fa fa-key icon"></i>
            <input class="input-field" id="password-2" type="password" name='new-password' placeholder="Retaper votre nouveau mot de passe" name="confirmPassword" oninput='validate();'>
        </div>
    
        <span id="pwd-length-2"></span>
        <span id="message"></span>
        
        <button class="btn" id="formSubmit" type="submit" disabled>Enregistrer</button>
    </form>
<?php 
}else{
    echo "et non ;)";
}
?> 

</body>
<script src="password.js"></script>


</html>