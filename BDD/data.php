<?php
// connaxtion PDO
include "db_connect.php";


// *===* Inséré les cordonnes du client a la base de données  *==*
function addCoordonneesClient($pseudo,$addresse,$numero_de_tel,$mdp,$PP) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO utilisateur (pseudo,addresse,numero_de_tel,mdp,photo_profil) VALUES(?,?,?,?,?)');
    $req->execute([$pseudo,$addresse,$numero_de_tel,$mdp,$PP]);
};

// *===* Recupere les cordonnes d'admin  *==*
function selectAdmin($nom, $mot_de_passe){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM Admin0 WHERE pseudo = ? AND mdp = ?');
    $req->execute([$nom, $mot_de_passe]);
    return $req->fetchAll();
};

// *===* Recupere les cordonnes du client  *==*
function selectClient($nom, $mot_de_passe){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE pseudo = ? AND mdp = ?');
    $req->execute([$nom, $mot_de_passe]);
    return $req ->fetchAll();
};

// *===* Recupere le nom du client  *==*
function selectClientNom($nom){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE pseudo = ? ');
    $req->execute([$nom]);
    return $req->fetchAll();
};
// *===* Recupere le nom du client avec son id *==*
function selectClientId($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE id = ? ');
    $req->execute([$id]);
    return $req->fetchAll();
};

// *===* Recupere le nom du Admin  *==*
function selectAdminNom($nom){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM Admin0 WHERE pseudo = ? ');
    $req->execute([$nom]);
    return $req->fetchAll();
};


// *===* On vérifie si le pseudo  du client existe *==*
function CheckExistClientName($nom){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM utilisateur WHERE pseudo=?");
    $stmt->execute([$nom]);
    $user = $stmt->fetch();
    if ($user) {
        return true;
    } else {
        return false;
    }
};

// *===* On vérifie si le mail du client existe *==*
function CheckExistClientMail($mail){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM utilisateur WHERE addresse=?");
    $stmt->execute([$mail]);
    $mail = $stmt->fetch();
    if ($mail) {
        return true;
    } else {
        return false;
    }
};

// *===* On vérifie si le numero de telephone du client existe *==*
function CheckExistClientPhone($tel){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM utilisateur WHERE numero_de_tel=?");
    $stmt->execute([$tel]);
    $tel = $stmt->fetch();
    if ($tel) {
        return true;
    } else {
        return false;
    }
};

// function selectAdminNom($nom){
//     global $pdo;
//     $req = $pdo->prepare('SELECT * FROM Admin0 WHERE pseudo = ? ');
//     $req->execute([$nom]);
//     return $req->fetchAll();
// };

// function selectClientAll(){
//     global $pdo;
//     $req = $pdo->query('SELECT * FROM utilisateur');
//     return $req->fetchAll();
// };

// *===* Inséré les donnes du produit a la base de données  *==*
function addProduit($nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO product(nom, description, descriptionMini, temperature, humidite,temps,stock,prix,si_solde,prix_solde,si_livraison,dateproduct) VALUES(?,?,?,?,?,?,?,?,?,?,?,NOW())');
    $req->execute([$nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison]);   
    return $pdo->lastInsertId();
};
// *===* Inséré les image a la base de données  *==*
function addImage($img){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO stock_image(url) VALUES(?)');
    $req->execute([$img]); 
    return $pdo->lastInsertId();  
};
// *===* Inséré l'id du image a la table de liason  *==*
function liaison_carousel($id_product,$id_images){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO carousel(id_image,id_carrousel) VALUES(?,?)');
    $req->execute([$id_images,$id_product]); 
}

// *===* Recupere les produits (liaison_carousel)  *==*
function getProduit(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM product');
    return $req->fetchAll();
};

// *===* Recupere les produits et image  *==*
function getProduitAndImage(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM carousel INNER JOIN product ON carousel.id_carrousel = product.id INNER JOIN stock_image ON carousel.id_image = stock_image.id');
    return $req->fetchAll();
}


// *===* Recupere  id du image (liaison_carousel) *==*
function getImage($id_product){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM stock_image INNER JOIN carousel ON id = id_image WHERE id_carrousel = ?');
    $req->execute([$id_product]);
    return $req->fetchAll();
};

function test(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM product WHERE id = "'.$_GET['product'].'"');
    return $req->fetchAll();
};

// *===* Recupere les produits rapport du ses id   *==*
function getProduitById($id){
    global $pdo; 
    $req = $pdo->prepare('SELECT * FROM product WHERE id = ?');
    $req->execute([$id]);
    return $req->fetchAll();
};


// *===* ???*==*
function getAllIdProduct(){
    global $pdo; 
    $req = $pdo->query('SELECT id,nom FROM product');
    return $req->fetchAll();
}

 // *===* Supprimer un produit/ et table de liaison  *==* 
function deleteProduit($id){
    global $pdo;
    $req = $pdo->prepare('DELETE FROM carousel WHERE id_carrousel = ?');
    $req->execute([$id]);
    
    $req = $pdo->prepare('DELETE FROM product WHERE id = ?');
    $req->execute([$id]);
};

 // *===* Supprimer un client  *==* 
function deleteClient($effacer){
    global $pdo;
    try{

        // $req = $pdo->prepare('DELETE FROM commande_product WHERE id_utilisateur = ?');
        // $req->execute([$effacer]);

        $req = $pdo->prepare('DELETE FROM utilisateur WHERE id = ?');
        $req->execute([$effacer]); 

    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    }
};
 // *===* Modifier les produits  *==* 
function updateProduit($nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison , $id){
    global $pdo;
    $req = $pdo->prepare('UPDATE product
    SET nom = ?,description = ?, descriptionMini = ?, temperature = ?,humidite = ? ,temps = ?,stock = ? ,prix = ?,si_solde = ?,prix_solde=?,si_livraison=?
    WHERE  id = ?');
    $req->execute([$nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison , $id]);
    return true;
};

// *===* Modifier les images  *==* 
function deletePhoto($id_product){
    global $pdo;
    $req = $pdo->prepare('DELETE FROM carousel WHERE id_carrousel = ?');
    $req->execute([$id_product]);
    return $req->fetchAll();
};

// // *===* Modifier les images  *==* 
// function updatePhoto($a,$id){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE stock_image
//     SET url = ? WHERE  id = ?');
//     $req->execute([$a,$id]);
//     return true;
// };


// *===* Modifier client  *==* 
function updateClient($nom, $NumeroDeTelephone, $mail  , $photo,$id ){
    global $pdo;
    $req = $pdo->prepare('UPDATE utilisateur
    SET pseudo = ?,numero_de_tel = ?, addresse = ? , photo_profil = ? WHERE id = ?');
    $req->execute([$nom, $NumeroDeTelephone, $mail  ,$photo,$id]);
   
}


// *===* Inséré les mails a la base de données  *==*
function InsertContact($name,$email,$comment){
    global $pdo;
    $req = $pdo->prepare("INSERT into contact (nom,email,comment,datemail) values (?,?,?,NOW());");
    $req->execute([$name,$email,$comment]);
};

// *===* Recupere les mail  *==*
function getAllMail(){ 
global $pdo;
$req = $pdo->query("SELECT * FROM contact");
return $req->fetchAll();
}

// *===* Supprimer les mail  *==*
function deleteMail($effacer){
    global $pdo; 
    $delete = $pdo->prepare("DELETE FROM contact WHERE id = ?");
    $delete->execute([$effacer]);
     }

 // *===* Inséré les Actualites a la base de données *==* 

function addActualites($title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO actualites(title, sous_title, card_description, mots_cles1, mots_cles2,mots_cles3,paragraph1,paragraph2,paragraph3,photo,author,Ladate) VALUES(?,?,?,?,?,?,?,?,?,?,?,NOW())');
    $req->execute([$title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author,]);   
    return $pdo->lastInsertId();
    };

 // *===* Recupere les Actualites  *==* 
  function getActualites(){
        global $pdo;
        $req = $pdo->query('SELECT * FROM actualites');
        return $req->fetchAll();
    };


    function actualite_view(){
        global $pdo;
        $req = $pdo->query('SELECT * FROM actualites WHERE id = "'.$_GET['actualites'].'"');
        return $req->fetchAll();
    };

 // *===* Supprimer les Actualites  *==* 
    function deleteActualites($effacer){
        global $pdo;
        $req = $pdo->prepare('DELETE FROM actualites WHERE id = ?');
        $req->execute([$effacer]);
    };

 // *===* Recupere les Actualites rapport du son id  *==* 
    function getctualitesById($id){
        global $pdo; 
        $req = $pdo->prepare('SELECT * FROM actualites WHERE id = ?');
        $req->execute([$id]);
        return $req->fetchAll();
    };

 // *===* Modifier les Actualites  *==* 
    function updateActualites($title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author, $id){
        global $pdo;
        $req = $pdo->prepare('UPDATE actualites
        SET title = ?,sous_title = ?, card_description = ?, mots_cles1 = ?,mots_cles2 = ? ,mots_cles3 = ?,paragraph1 = ? ,paragraph2 = ?,paragraph3 = ?,photo=?,author=?
        WHERE  id = ?');
        $req->execute([$title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author, $id]);
        
    };

// *===* Resent le password of client  *==* 
    function updateClientMDP($mdp, $addresse){
        try{
            global $pdo;
            $req = $pdo->prepare('UPDATE utilisateur SET mdp = ? WHERE addresse = ?');
            $req->execute([$mdp, $addresse]);
        }catch(Exception $e){
        // en cas d'erreur :
         echo " Erreur ! ".$e->getMessage();
         echo $req;
      }
    }
// *===* Recupere toutes les commandes *===*
    function selectAllCommande(){
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM commande_product  INNER JOIN transactions ON commande_product.id_transaction = transactions.id INNER JOIN product ON commande_product.id_produit = product.id INNER JOIN carousel ON product.id = carousel.id_carrousel INNER JOIN stock_image ON carousel.id_image = stock_image.id ORDER BY id_transaction ASC' );
        $req->execute();
        return $req->fetchAll();
    };

// *===* Recupere la commande du client  *==* 
    function selectClientCommande($id){
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM commande_product INNER JOIN transactions ON commande_product.id_transaction = transactions.id WHERE id_utilisateur = ? ');
        $req->execute([$id]);
        return $req->fetchAll();
    };


// *===* Recupere toutes les commandes par apports du id de clients*===*
function selectAllCommandeByidOFClient($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM commande_product INNER JOIN product ON commande_product.id_produit = product.id INNER JOIN carousel ON product.id = carousel.id_carrousel INNER JOIN stock_image ON carousel.id_image = stock_image.id INNER JOIN transactions ON commande_product.id_transaction = transactions.id WHERE id_utilisateur = ? ');
   $req->execute([$id]);
    return $req->fetchAll();
};




    // *===* Recupere just les prix avec la solde  *==* 
    function getProduitAndImageAndSolde(){
        global $pdo;
        $req = $pdo->query('SELECT * FROM carousel INNER JOIN stock_image ON carousel.id_image = stock_image.id INNER JOIN product ON carousel.id_carrousel = product.id WHERE si_solde = 0  ');
        return $req->fetchAll();
    }
    

 // *===* TOTAL de produits  *==* 
 function TotalProduits(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM product ');
    return $req->fetchAll();
}

 // *===* TOTAL clients  *==* 
 function TotalClients(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM utilisateur ');
    return $req->fetchAll();
}
    
 // *===* TOTAL commandes  *==* 
function TotalCommandes(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM commande_product ');
    return $req->fetchAll();
}


 // *===* TOTAL posts  *==* 
 function TotalPosts(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM actualites ');
    return $req->fetchAll();
}

// *===* TOTAL categories  *==* 
function TotalCategorie(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM categorie ');
    return $req->fetchAll();
}

// *===* TOTAL Messages  *==* 
function TotalMessages(){
    global $pdo;
    $req = $pdo->query('SELECT count( * ) as  total  FROM contact ');
    return $req->fetchAll();
}

// *===* ajouter categorie  *==* 
function addCategorie($nom,$description){
    global $pdo;
    $req = $pdo->prepare("INSERT into categorie (nom,description) values (?,?);");
    $req->execute([$nom,$description]);
}

// *===* selectioner categorie  *==* 
function selectCategorie(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM categorie');
    return $req->fetchAll();
}

// *===* modifier categorie  *==*
function updateCategorie($nom,$description,$id){
    global $pdo;
    $req = $pdo->prepare("UPDATE categorie SET nom = ?, description = ? WHERE id = ?");
    return $req->execute([$nom,$description,$id]);
}

// *===* supprimer categorie  *==*
function deleteCategorie($id){
    global $pdo;
    $req = $pdo->prepare('DELETE FROM categorie WHERE id = ?');
    return $req->execute([$id]);
}

// *===* ajouter dans la table de liaison de categorie (id de produit et id de categorie) *==*
function liaison_Categorie($id_produit,$id_categorie){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO categorie_product(id_produit,id_categorie) VALUES(?,?)');
    $req->execute([$id_produit,$id_categorie]); 
}

// *===* Modifier la table de liaison categorie *==
function liaison_Categorie_update($id_categorie,$id_produit){
    try{
        global $pdo;
        $req = $pdo->prepare("UPDATE categorie_product SET id_categorie = ? WHERE id_produit = ?");
        $req->execute([$id_categorie,$id_produit]);
    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    } 
}

// *===* verifier si la categorie exist *==
function checkExistCatId($id){
    try{
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM categorie_product WHERE id_produit = ?');
        $req->execute([$id]);
        $exist = $req->fetch();
        if ($exist) {
        return true;
        } else {
        return false;
        } 
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

function catId($id){
    try{
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM categorie INNER JOIN categorie_product ON categorie.id = categorie_product.id_categorie WHERE categorie_product.id_produit = ?');
        $req->execute([$id]);
        return $req->fetchAll(); 
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}